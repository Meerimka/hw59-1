import React from 'react';
import './Film.css';

const  Film = props => (
    <div className="AddTask">
        <h1>{props.title}</h1>
        <p>
            <input type="text" value={props.value} onChange={(e) => props.changeHandler(e.target.value)}/>
            <button onClick={props.addHandler}>Add</button>
        </p>
    </div>

);
export default Film;