import React, {Component} from 'react';
import './FilmTask.css';

class  FilmTask extends Component {

    shouldComponentUpdate(nextProps, nextState){
        return nextProps.film !== this.props.film ;
    }

        render() {
        return (
            <div className="Task">
                <input type="text" value={this.props.film} onChange={this.props.editHandler}/>
                <button onClick ={this.props.remove}>X</button>
            </div>
        )
    }
};
export default FilmTask;