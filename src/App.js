import React, { Component } from 'react';
import './App.css';
import Film from "./components/AddFilmForm/AddFilm";
import FilmTask from "./components/Films/FilmTasks";

class App extends Component {
    state = {
        currentId: 3,
        films: [
            {film: 'Harry Poter',  id: 1},
            {film: 'Iron Man', id: 2},
            {film: 'War of Stars',  id: 3}
        ],
        showTasks: false,
        value: ''
    };

    changeHandler = value => {
        this.setState({value})
    };

    removeHadler = id =>{
        const films = [...this.state.films];
        const filmId = films.findIndex(film => film.id === id);

        films.splice(filmId, 1);
        this.setState({films: films});
    };

    addHandler = () => {
        const newFilm = {film: this.state.value, id: this.state.currentId + 1};
        if(newFilm !== ' '){
            let films = [...this.state.films, newFilm];
            this.setState({films, currentId: this.state.currentId + 1, value: ""})
        } else{
          alert('Please enter a film')
        }


    };


    editHandler = (event, index) => {
      const films = this.state.films;
      films[index].film = event.target.value;
      this.setState({
          films
      })
    }


    render() {
    return (
      <div className="App">
        <Film addHandler={this.addHandler} value={this.state.value} changeHandler={this.changeHandler}/>
          <h3>To watch films:</h3>
          {this.state.films.map((film, key) => <FilmTask remove={()=> this.removeHadler(film.id)} editHandler={(event) => this.editHandler(event, key)} film={film.film} key={key}/>)}
      </div>
    );
  }
}

export default App;
